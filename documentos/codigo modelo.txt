-- Generado por Oracle SQL Developer Data Modeler 21.4.2.059.0838
--   en:        2023-04-15 22:09:34 COT
--   sitio:      Oracle Database 11g
--   tipo:      Oracle Database 11g



-- predefined type, no DDL - MDSYS.SDO_GEOMETRY

-- predefined type, no DDL - XMLTYPE

CREATE TABLE persona (
    correo   VARCHAR2(100) NOT NULL,
    nombre   VARCHAR2(100) NOT NULL,
    password VARCHAR2(100) NOT NULL
);

ALTER TABLE persona ADD CONSTRAINT persona_pk PRIMARY KEY ( correo );

CREATE TABLE producto (
    id_producto INTEGER NOT NULL,
    marca       VARCHAR2(100) NOT NULL,
    nombre      VARCHAR2(100) NOT NULL,
    precio      FLOAT(100) NOT NULL,
    imagen      VARCHAR2(100) NOT NULL,
    stock       INTEGER NOT NULL,
    descripcion VARCHAR2(100) NOT NULL
);

ALTER TABLE producto ADD CONSTRAINT producto_pk PRIMARY KEY ( id_producto );

CREATE TABLE venta (
    id_venta             INTEGER NOT NULL,
    fecha_venta          VARCHAR2(100) NOT NULL,
    catidad              INTEGER NOT NULL,
    valor                FLOAT(100) NOT NULL,
    persona_correo       VARCHAR2(100) NOT NULL,
    producto_id_producto INTEGER NOT NULL
);

ALTER TABLE venta ADD CONSTRAINT venta_pk PRIMARY KEY ( id_venta,
                                                        fecha_venta );

ALTER TABLE venta
    ADD CONSTRAINT venta_persona_fk FOREIGN KEY ( persona_correo )
        REFERENCES persona ( correo );

ALTER TABLE venta
    ADD CONSTRAINT venta_producto_fk FOREIGN KEY ( producto_id_producto )
        REFERENCES producto ( id_producto );



-- Informe de Resumen de Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                             3
-- CREATE INDEX                             0
-- ALTER TABLE                              5
-- CREATE VIEW                              0
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          0
-- CREATE MATERIALIZED VIEW                 0
-- CREATE MATERIALIZED VIEW LOG             0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- 
-- ORDS DROP SCHEMA                         0
-- ORDS ENABLE SCHEMA                       0
-- ORDS ENABLE OBJECT                       0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
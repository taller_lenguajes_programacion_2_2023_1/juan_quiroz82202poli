import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserRegisterService {
  constructor() { }
  
  private async setUser(userInfo: any){
    //@ts-ignore
    const users = JSON.parse(sessionStorage.getItem('users')) || [];
    sessionStorage.setItem('users', JSON.stringify([...users, userInfo]))
  }
  
  async registerUser(userInfo: any){
    debugger
    await this.setUser(userInfo);
    // aqui vamos a hacer la petición
  }
}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { Login } from '../../interfaces/login';

export interface User{
		id: number,
		email: String,
		first_name: String,
		last_name: String,
		avatar: String
}

@Injectable()
export class AuthService {
  
  private url: string = 'https://dummyjson.com/auth/login';
  private headers: HttpHeaders = new HttpHeaders({'Content-type': 'application/json'});

  constructor(
		public _http: HttpClient,
		private router: Router
	) { }

  public auth(body: Login): Observable<any>{

		const httpOptions = {
		  headers: this.headers
		};
    
		//return this._http.post(this.url, body, httpOptions);
		return this.getUserInfo();
	}

  public getUserInfo(){
	const user:User={
		"id": 8,
		"email": "lindsay.ferguson@reqres.in",
		"first_name": "Lindsay",
		"last_name": "Ferguson",
		"avatar": "https://reqres.in/img/faces/8-image.jpg"
	}
	return of (user)
  }
}

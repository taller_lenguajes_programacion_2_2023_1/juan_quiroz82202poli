import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';

@Injectable()
export class ProductsService {

  private url: string = 'assets/json/telescopios.json';
  private headers: HttpHeaders = new HttpHeaders({'Content-type': 'application/json', 'Authorization': "Bearer "+sessionStorage.getItem('token')});

  constructor(
		public _http: HttpClient,
		private router: Router
	) { }

  public getAllProducts(): Observable<any>{

		const httpOptions = {
		  headers: this.headers
		};
    
		return this._http.get(this.url, httpOptions);
	}
}

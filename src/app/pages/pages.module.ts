import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import { AuthGuard } from '../core/guards/auth/auth.guard';

const routes: Routes =[
  {
    path: 'dashboard',
    canActivate:[AuthGuard], 
    children: [
      {
        path: '',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      }
    ]
  },
  {
    path: 'auth',
    children: [
      {
        path: '',
        loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
      }
    ]
  },
  {
    path: 'register',
    children: [
      {
        path: '',
        loadChildren: () => import('./register/register.module').then(m => m.RegisterModule)
      }
    ]
  }, 
  {
    path: 'detail',
    children: [
      {
        path: '',
        loadChildren: () => import('./detail/detail.module').then(m => m.DetailModule)
      }
    ]
  },
  {
    path: 'soon',
    children: [
      {
        path: '',
        loadChildren: () => import('./soon/soon.module').then(m => m.SoonModule)
      }
    ]
  },  
  {path: '**', redirectTo: 'auth/login'},
];  

@NgModule({
  providers:[AuthGuard],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule
  ]
})
export class PagesModule { }

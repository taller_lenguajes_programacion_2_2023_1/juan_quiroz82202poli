import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {SoonComponent } from "./components/soon/soon.component";

const routes: Routes =[
    {
        path: '',
        component: SoonComponent
    }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports:[
    RouterModule
  ]
})
export class SoonRouting { }

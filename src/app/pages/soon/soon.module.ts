import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SoonRouting } from './soon.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { SoonComponent } from './components/soon/soon.component';

@NgModule({
  schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ],
  declarations: [
    SoonComponent
  ],
  imports: [
    CommonModule,
    SoonRouting,
    SharedModule
  ]
})
export class SoonModule { }

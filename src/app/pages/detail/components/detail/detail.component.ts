import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  marca: string = '';
  nombre: string = '';
  precio: number = 0;
  imagen: string = '';
  stock: number = 0;
  descripcion: string = '';

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    const marca = this.route.snapshot.queryParamMap.get('marca');
    if (marca) {
      this.marca = marca;
    }

    const nombre = this.route.snapshot.queryParamMap.get('nombre');
    if (nombre) {
      this.nombre = nombre;
    }

    const precio = this.route.snapshot.queryParamMap.get('precio');
  if (precio !== null) {
  const precioNum = parseFloat(precio);
  if (!isNaN(precioNum)) {
    this.precio = precioNum;
    }
  }


    const imagen = this.route.snapshot.queryParamMap.get('imagen');
    if (imagen) {
      this.imagen = imagen;
    }

    const stock = this.route.snapshot.queryParamMap.get('stock');
    if (stock !== null) {
      const stockNum = parseInt(stock);
      if (!isNaN(stockNum)) {
        this.stock = stockNum;
      }
    }

    const descripcion = this.route.snapshot.queryParamMap.get('descripcion');
    if (descripcion) {
      this.descripcion = descripcion;
    }
  }


  }


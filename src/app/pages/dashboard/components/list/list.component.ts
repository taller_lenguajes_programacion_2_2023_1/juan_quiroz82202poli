import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/core/services/products/products.service';

declare var window: any;

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit{
  public formModal: any;
  public errorService: string | undefined;
  public listProducts: any;

  constructor(
    private productsService: ProductsService) {
    
  }

  ngOnInit(): void {
    this.callProcuts();
    this.formModal = new window.bootstrap.Modal(
      document.getElementById('myModal')
    );  
    
  }
 
  public callProcuts(): void{
    this.productsService.getAllProducts().subscribe(
      data => {
        console.log("data: ",data)
        this.listProducts = data
      }
    );
  }

}

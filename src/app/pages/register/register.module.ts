import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterRouting } from './register.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { RegisterComponent } from './components/regis/register.component';

@NgModule({
  schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ],
  declarations: [
    RegisterComponent
  ],
  imports: [
    CommonModule,
    RegisterRouting,
    SharedModule
  ]
})
export class RegisterModule { }

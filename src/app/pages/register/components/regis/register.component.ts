import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { UserRegisterService } from 'src/app/core/services/userRegister/user-register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  public formModal: any;
  public errorService: string | undefined;
  public registerForm = new FormGroup({
    userName: new FormControl(),
    email:  new FormControl(),
    password:  new FormControl(),
    confirm:  new FormControl(),
  });
  
  constructor(
   private _userRegister : UserRegisterService
  ){}

  public submit(): void {
    debugger
    console.log(this.registerForm.value)
    this._userRegister.registerUser(this.registerForm.value).then(()=> {
      debugger
      alert('Registro guardado!');
      this.registerForm.reset();
    })
  }
}


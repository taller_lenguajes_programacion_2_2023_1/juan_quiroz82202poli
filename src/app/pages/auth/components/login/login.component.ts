import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Login } from 'src/app/core/interfaces/login';
import { AuthService } from 'src/app/core/services/auth/auth.service';

declare var window: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public formModal: any;
  public errorService: string | undefined;

  public loginForm: FormGroup = new FormGroup({
    username: new FormControl(null, [
      Validators.required,
      Validators.pattern("^[a-zA-Z0-9]+$"),
      Validators.minLength(3),
      Validators.maxLength(20)]),
    password: new FormControl(null, [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(25)]),
  });

  public errorMessages = {
    'username': [
      { type: 'required', message: 'El campo es requerido.' },
      { type: 'pattern', message: 'El campo es numerico.' },
      { type: 'minlength', message: 'Minimo es de 3 caracteres.' },
      { type: 'maxlength', message: 'Maximo es de 15 caracteres.' },
    ],
    'password': [
      { type: 'required', message: 'El campo es requerido.' },
      { type: 'pattern', message: 'The password if minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character.' },
      { type: 'minlength', message: 'Minimo es de 5 caracteres.' },
      { type: 'maxlength', message: 'Maximo es de 25 caracteres.' },
    ]
  };

  constructor(
    private router: Router,//cambios de pagina
    private authService: AuthService) { }

  ngOnInit(): void {
    sessionStorage.clear();
    this.formModal = new window.bootstrap.Modal(
      document.getElementById('myModal')
    );  
  }

  public submit(): void {
    this.authService.auth(this.loginForm.value).subscribe(
      (res: any) => {
        console.log(res);
        sessionStorage.setItem("userInfo",JSON.stringify(res)); //(nombre, ruta)
        let expireDate = new Date().getTime() + (1000 * res.expires_in);
        sessionStorage.setItem('token', res.token);
        this.router.navigate(['/dashboard']);
      },
      err => {
        console.log(err)
        this.errorService = err.error.message;  
        this.formModal.show();
      }
    );
  }
}

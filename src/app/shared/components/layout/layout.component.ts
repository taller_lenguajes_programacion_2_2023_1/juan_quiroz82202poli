import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth/auth.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})

export class LayoutComponent implements OnInit {
  userInfo:any={};
  constructor(
    private authService: AuthService //injeccion de dependencia (funcionalidad)
  ) { }
  ngOnInit(): void {
    //this.authService.auth().subscribe((res:any)=>{}) //para autenticar
    const info= sessionStorage.getItem("userInfo")
    // @ts-ignore
    this.userInfo=JSON.parse(info) || {};//si no llega nada manda objeto vacio por defecto
  }
}


import { Component, Input, OnInit } from '@angular/core';

declare var window: any;

interface Window {
  bootstrap:any;
}

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  @Input() modal: any;

  @Input() message: string | undefined;

  ngOnInit(): void {
      
  }

  public saveSomeThing(): void {
    this.modal.hide();
  }
}
